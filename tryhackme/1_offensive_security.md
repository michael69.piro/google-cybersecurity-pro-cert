# Ejercicio

https://tryhackme.com/room/introtooffensivesecurity

Uso de bobuster para detectar vulnerabilidades en paginas no protegidas. Se busca mediante el dominio el acceso a otras rutas.

Se accede a http://fakebank.com/bank-transfers y se realiza una transferencia de una cuenta a otra. Siendo esta exitosa.

````
gobuster -u http://fakebank.com -w wordlist.txt dir

http://fakebank.com/bank-transfers
http://fakebank.com/images


```

## Cyber Carrers

The cyber careers room goes into more depth about the different careers in cyber. However, here is a short description of a few offensive security roles:

Penetration Tester - Responsible for testing technology products for finding exploitable security vulnerabilities.

Red Teamer - Plays the role of an adversary, attacking an organization and providing feedback from an enemy's perspective.

Security Engineer - Design, monitor, and maintain security controls, networks, and systems to help prevent cyberattacks.
