# Networks basics.

## Dev notes: 
    - How to do practical labs?

## Expected to learn:
    - Structure of a network
    - Standar networking tools
    - Cloud networks
    - TCP/IP

## What is a network? 
Is a group of conecting devices.

Devices communicate via wireless or cable.

LAN - WAN
Local Area Network. Smalls networks
Wide Area Network. Large netwoks

## Useful skill for network security.
programing
how computer works
components to create infrastructure
operative systems
memory, kernel
log debuging
network trafic analysis
various aplicacion of networks layers.
how to configure a firewall?

## Networks tools
Hub -> networks device to broadcast signal
Switch -> specific device to connect the network
Router -> connect multiple network devices
Modems -> Device to connect router to internet

Virtualization Tools 
    - Cloud computing
    - Cloud services

Devices has uniques IDs 
    - Mac address 
    - IP address

Firewall 
    - Can restrict specific incoming/outcoming network trafic. 
    - Configure security rules.

Servers
    - Provide service to others devices on the network
    - Devices connect to a server are called clients.

Hubs and Switches
    - Cirect traffic on a local network
    - Provide common point of connection for all devices

Routers 
    - Between networks and direct traffic, based on IP address.
    - IP Headers.
    - Forward packets to the next router.
    - Routers can include firewalls to allow or block incoming traffic.

Modem 
    - ISP

Wireless Access Point

### Using networ diagrams as a secutiry analyst

Network architecture
How to design networks

## Cloud Networks
collection of servers

Cloud security

## Cloud Service Provider (CSP)
IaaS -> Infrastructure as service, refers to use virtual computers components offered by the CSP. Containers, Storage.

PaaS -> Platform as a service, refers to tools that application developers can use to design customs applications. OS, Databases.

Software-defined networks

## Benefits of cloud computing.
Reliability
Cost
Scalability

## Network communication

packet, unit of information

## TCI/IP model
Transmission Control Protocol 
Internet Protocol
Port

the four layers of TCP/IP model
    - Networks access layer
    - Internet layer
    - Transport layer
    - Application layer

https://www.coursera.org/learn/networks-and-network-security/supplement/SXl0z/learn-more-about-the-tcp-ip-model

## OSI model
https://www.coursera.org/learn/networks-and-network-security/supplement/YbKL0/the-osi-model

## Network Protocols 
TCP create a connection and share data
ARP Adress Resolution Protocol -> network protocol used to determinate the MAC address of the next router or device on the path

## HTTPS 

## DNS
Networks protocol that translates internet domains names into IP addresses

Para acceder a una web accedemos por 4 protocolos
    - TCP
    - ARP
    - HTTPS
    - DNS

## IEEE 802.11 (WIFI)

## WPA Wifi Protected Area
WEP
WPA
WPA2
WPA3

## Firewalls
networks security device that monitors traffic to and from your network.

Port Filtering -> block certain ports.

Hardware firewall, inspect each data packet
Software firewall, installed on the computer.
Cloud base Firewall -> hosted by a cloud service provider.

Stateful -> keeps track of information passing through it and proactively filters out threats

stateless -> operate based on predefined rules and dows not keep track of information from data packets

NGFWs
    - Deep packet inspection
    - Intrusion protection
    - Threat intelligence

## VPNs Virtual private network
a Network security service that changes your public IP address and hides your virtual location so that you can keep your data private when you are using a public network like the internet

Encapsulation

VPN Tunnel

## Security zone 
a segment of a network that protects the internal netwokr from the internet

Network segmentation
a security technique that divides the network into segments

subnet

uncontrolled zone
controlled zone

areas in the controlled zone 
    - Demilitarized zone (DMZ)
    - Internal network
    - Rescticted zone

## Subnetting and CIDR

## Proxy server
a server that fulfills the request of a client by forwarding them in to other servers

Forward proxy server
regulates and restricts a person's access to the internet

Reverse proxy server
regulates and restricts the internet access to an internet server

Communication protocols
Used to establish connections between servers. TCP, UDP, SMTP

Management protocols 
Used to troubleshoot networks issues, ICMP.

Security protocols, provides encryption data in transit. SSL/TLS

HTTP, DNS, ARP

